(cl:let* ((quicklisp-init (cl:merge-pathnames "quicklisp/setup.lisp"
                                              (cl:user-homedir-pathname))))
  (cl:when (cl:probe-file quicklisp-init)
    (cl:load quicklisp-init)))

(cl:in-package #:cl-user)
(ql:quickload '(:unix-opts :demo-derive :uiop :cs-maps))

(delete-package :quicklisp)

(defun find-path-to-maps ()
  (elt (uiop:command-line-arguments) (1+ (position "--path-to-maps"
                                                   (uiop:command-line-arguments)
                                                   :test #'equal))))

(setf cs-maps:*maps-path* (find-path-to-maps))
(assert cs-maps:*maps-path*)
(map nil (lambda (map &aux (outline (cs-maps:outline map)))
           (cs-maps:point->zone outline 0.0 0.0 0.0))
     cs-maps:*all-maps*)

(defun parse-numbers-list (input)
  (mapcar #'parse-integer (cl-ppcre:split " " input)))

(defun parse-producers (input)
  (cl-ppcre:split " " input))

(defun parse-default-behavor-on-existing (string)
  (alexandria:eswitch (string :test 'equal)
    ("replace" :replace)
    ("new" :new)
    ("use" :use)))

(defun parse-boolean (string)
  (alexandria:eswitch (string :test 'equal)
    ("true" t)
    ("false" nil)))


(define-condition timelimit-reached (demo-derive.produce:unrecoverable-error)
  ())


(opts:define-opts
  (:name :timelimit
   :arg-parser #'parse-integer
   :description "Timelimit of the products group."
   :default nil
   :long "timelimit")
  (:name :config
   :arg-parser #'identity
   :description "Path to config."
   :short #\c
   :long "config")
  (:name :top-level-memo-table
   :long "top-level-memo-table"
   :default nil
   :arg-parser  #'parse-boolean)
  (:name :memoization
   :arg-parser #'parse-boolean
   :description "Pass true to enable memoization."
   :short #\m
   :default nil
   :long "memoization")
  (:name :demos
   :arg-parser #'parse-numbers-list
   :description "List of demos to process."
   :short #\d
   :long "demos")
  (:name :external-ids
   :arg-parser #'parse-numbers-list
   :description "List of external ids to process."
   :short #\d
   :long "external-ids")
  (:name :producers
   :arg-parser #'parse-producers
   :short #\p
   :long "producers")
  (:name :default-behavior-on-existing
   :default :use
   :arg-parser #'parse-default-behavor-on-existing
   :long "default-behavior-on-existing")
  (:name :automatic-deletion
   :default nil
   :arg-parser #'parse-boolean
   :long "automatic-deletion")

  (:name :lifetime
   :arg-parser #'parse-integer
   :default nil
   :long "lifetime")
  (:name :replace-products-groups-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "replace-products-groups-ids")
  (:name :new-products-groups-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "new-products-groups-ids")
  (:name :use-products-groups-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "use-products-groups-ids")

  (:name :replace-products-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "replace-products-ids")
  (:name :use-products-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "use-products-ids")
  (:name :new-products-ids
   :arg-parser #'parse-numbers-list
   :default (list)
   :long "new-products-ids")

  (:name :replace-products
   :arg-parser #'parse-producers
   :long "replace-products")
  (:name :use-products
   :arg-parser #'parse-producers
   :long "use-products")
  (:name :new-products
   :arg-parser #'parse-producers
   :long "new-products")

  (:name :allow-single-demo-failures
   :default nil
   :long "allow-single-demo-failures"
   :arg-parser #'parse-boolean)

  (:name :information
   :default :null
   :long "information"
   :arg-parser #'identity)

  (:name :just-send-producers
   :default nil
   :long "just-send-producers"
   :arg-parser #'parse-boolean)

  (:name :status-path
   :default nil
   :long "status-path"
   :arg-parser #'identity))

(defvar *in-progress-p* nil)

(defun main ()
  (trivial-signal:signal-handler-bind
   ((15 (lambda (c) (declare (ignore c))
          (error 'demo-derive.produce:terminated))))
   (handler-case
       (metabang-bind:bind (((:values options free-args) (opts:get-opts))
                            (config (getf options :config))
                            (*in-progress-p* t)
                            (lifetime (getf options :lifetime))
                            (producers (getf options :producers))
                            (automatic-deletion (getf options :automatic-deletion))
                            (default-behavor-on-existing
                             (getf options :default-behavior-on-existing))
                            (information (getf options :information))
                            (external-ids (getf options :external-ids))
                            (top-level-memo-table (getf options :top-level-memo-table))
                            (allow-single-demo-failures (getf options :allow-single-demo-failures))
                            (replace-products-ids (getf options :replace-products-ids))
                            (use-products-ids (getf options :use-products-ids))
                            (new-products-ids (getf options :new-products-ids))
                            (replace-products (getf options :replace-products))
                            (use-products-groups (getf options :use-products-groups-ids))
                            (new-products-groups (getf options :new-products-groups-ids))
                            (replace-products-groups (getf options :replace-products-groups-ids))
                            (new-products (getf options :new-products))
                            (use-products (getf options :use-products))
                            (demo-derive.produce:*memoization* (getf options :memoization))
                            (demos (getf options :demos))
                            (just-send-producers (getf options :just-send-producers))
                            (status-path (getf options :status-path))
                            (main-thread (bt:current-thread))
                            (timelimit (getf options :timelimit))
                            (timelimit-thread
                             (unless (null timelimit)
                               (bt:make-thread
                                (lambda ()
                                  (sleep timelimit)
                                  (bt:interrupt-thread main-thread
                                                       (lambda ()
                                                         (when *in-progress-p*
                                                           (error 'timelimit-reached)))))))))
         (declare (ignore free-args))
         (when (null config)
           (error "No config option provided!"))
         (flet ((impl ()
                  (setf cl-progress-bar:*progress-bar-enabled* t)
                  (handler-bind ((demo-derive.produce:product-not-ready-yet
                                   (lambda (condition) (declare (ignore condition))
                                     (invoke-restart 'demo-derive.produce:retry-later)))
                                 (warning
                                   (lambda (condition) (declare (ignore condition))
                                     (invoke-restart 'muffle-warning)))
                                 (demo-derive.produce:single-demo-failed
                                   (lambda (condition) (declare (ignore condition))
                                     (if allow-single-demo-failures
                                         (invoke-restart 'demo-derive.produce:skip-demo-id)
                                         (invoke-restart 'demo-derive.produce:fail-product))))
                                 (demo-derive.produce:already-existing
                                   (lambda (condition)
                                     (let* ((producer (demo-derive.produce:producer condition))
                                            (name (demo-derive.produce:name producer))
                                            (product (demo-derive.produce:product condition))
                                            (product-id (demo-derive.produce:id product))
                                            (product-group-id (demo-derive.produce:products-group-id product)))
                                       (cond ((eql (demo-derive.produce:success product) :null)
                                              (invoke-restart 'demo-derive.produce:use-existing-product))

                                             ((member product-id use-products-ids)
                                              (invoke-restart 'demo-derive.produce:use-existing-product))
                                             ((member product-id new-products-ids)
                                              (invoke-restart 'demo-derive.produce:create-new-product))
                                             ((member product-id replace-products-ids)
                                              (invoke-restart 'demo-derive.produce:replace-existing-product))

                                             ((member product-group-id use-products-groups)
                                              (invoke-restart 'demo-derive.produce:use-existing-product))
                                             ((member product-group-id new-products-groups)
                                              (invoke-restart 'demo-derive.produce:create-new-product))
                                             ((member product-group-id replace-products-groups)
                                              (invoke-restart 'demo-derive.produce:replace-existing-product))

                                             ((member name replace-products :test 'equal)
                                              (invoke-restart 'demo-derive.produce:replace-existing-product))
                                             ((member name new-products :test 'equal)
                                              (invoke-restart 'demo-derive.produce:create-new-product))
                                             ((member name use-products :test 'equal)
                                              (invoke-restart 'demo-derive.produce:use-existing-product))

                                             ((eq default-behavor-on-existing :new)
                                              (invoke-restart 'demo-derive.produce:create-new-product))
                                             ((eq default-behavor-on-existing :replace)
                                              (invoke-restart 'demo-derive.produce:replace-existing-product))
                                             ((eq default-behavor-on-existing :use)
                                              (invoke-restart 'demo-derive.produce:use-existing-product)))))))
                    (let ((demo-derive.produce:*config* (demo-derive.produce:parse-config config)))
                      (demo-derive.produce:find-and-register-python-producers)
                      (when just-send-producers
                        (demo-derive.produce:with-postgres-connection (demo-derive.produce:*config*)
                          (tagbody start
                             (handler-case (postmodern:with-transaction (nil :serializable)
                                             (demo-derive.produce:send-producers-collection-to-database*
                                              demo-derive.produce:*producers-collection*))
                               (postmodern:database-error (e)
                                 (if (member (postmodern:database-error-code e)
                                             '("40001" "40P01")
                                             :test #'equal)
                                     (go start)
                                     (error e))))))
                        (cl-user::quit))
                        (when top-level-memo-table
                          (setf demo-derive.produce:*top-level-memo-table*
                                (make-hash-table :test 'equal)))
                        (when (null producers)
                          (error "No producers provided!"))
                        (when (and (null demos)
                                   (null external-ids))
                          (error "No demos provided!"))
                        (unless (= (+ (length replace-products)
                                      (length new-products)
                                      (length use-products))
                                    (serapeum:~> (append replace-products
                                                         new-products
                                                         use-products)
                                                 (remove-duplicates :test #'equal)
                                                 length))
                          (error "Conflicting products policy lists!"))
                      (demo-derive.produce:with-postgres-connection (demo-derive.produce:*config*)
                        (demo-derive.produce:fetch-producers-collection-from-database*
                         demo-derive.produce:*producers-collection*)
                        (map nil #'demo-derive.produce:find-producer new-products)
                        (map nil #'demo-derive.produce:find-producer use-products)
                        (map nil #'demo-derive.produce:find-producer replace-products)
                        (serapeum:~>>
                         (if (null external-ids)
                             nil
                             (postmodern:with-transaction (nil :serial)
                               (postmodern:query (:select 'id :from 'demos_demo
                                                   :where (:in 'external_id (:set external-ids))))))
                         alexandria:flatten
                         (append demos)
                         remove-duplicates
                         (demo-derive.produce:invoke-with-sequence (demo-derive.produce:extra-arguments)
                                                                   producers _
                                                                   :lifetime lifetime
                                                                   :information information
                                                                   :automatic-deletion automatic-deletion)))))))
           (unwind-protect
                (progn
                  (if (null status-path)
                      (impl)
                      (alexandria:with-output-to-file (demo-derive.produce:*status-stream* status-path
                                                                                           :if-exists :append
                                                                                           :if-does-not-exist :create)
                        (impl)))))
           (cl-user::quit)))
     (demo-derive.produce:terminated (e) (declare (ignore e))
       (sb-ext:exit :code 5 :timeout 10))
     (demo-derive.produce:unrecoverable-error (e)
       (format *error-output* "~a~%" e)
       (sb-ext:exit :code 10 :timeout 10)))))


(sb-ext:save-lisp-and-die "demo-derive"
                          :purify t
                          :executable t
                          :toplevel #'main)
