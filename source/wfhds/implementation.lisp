(cl:in-package #:demo-derive.wfhds)


(defvar *round*)


(defvar *weapon-type-intervals*
  (dict 1 64 ;pistol
        2 32 ;rifle
        3 32 ;smg
        4 384 ;sniper
        ))


(defvar *weapon-types*
  (dict 1 1
        2 1
        3 1
        4 1
        5 1
        6 1
        7 1
        8 1
        9 1
        10 1
        101 3
        102 3
        103 3
        104 3
        105 3
        106 3
        107 3
        201 6
        202 6
        203 6
        204 6
        205 5
        206 5
        301 2
        302 2
        303 2
        304 2
        305 2
        306 4
        307 2
        308 2
        309 4
        310 4
        311 4
        401 9
        402 10
        403 10
        404 10
        405 7
        406 10
        407 10
        501 8
        502 8
        503 8
        504 8
        505 8
        506 8))


(defparameter *target-columns*
  '(target-health target-armor
    target-x target-y target-z target-pitch target-yaw target-vel0
    target-vel1 target-vel2 target-boolean-mask
    target-zoom-level))


(defparameter *attacker-columns*
  '(playerhurt-id attacker-steam-id attacker-x attacker-y attacker-z attacker-pitch
    attacker-yaw health-dmg armor-dmg hitgroup attacker-side target-side target-steam-id
    attacker-vel0 attacker-vel1 attacker-vel2
    attacker-boolean-mask attacker-zoom-level
    attacker-health attacker-armor))


(defun all-target-positions-at-ticks (demo-id product-id positions sequence
                                      &aux (round (~> sequence first-elt first)))
  (cl-ds.utils:transform #'second sequence)
  (setf sequence (sort sequence #'< :key #'second))
  (let* ((sequence-length (~> sequence
                              (cl-ds.alg:on-each #'third)
                              cl-ds.alg:distinct
                              cl-ds.alg:count-elements))
         (sequence-id (~> sequence first-elt third))
         (all-ticks (~> (cl-ds.alg:distinct sequence :test #'= :key #'second)
                        cl-ds.alg:to-vector))
         (bullet-numbers (~> (cl-ds.alg:zip #'cons
                                            all-ticks
                                            (cl-ds:iota-range))
                             (cl-ds.alg:to-hash-table
                              :hash-table-key (compose #'second #'car)
                              :size sequence-length
                              :hash-table-value #'cdr)))
        (all-target-steams (~> sequence
                               (cl-ds.alg:on-each (lambda (list) (elt list 15)))
                               (cl-ds.alg:without (curry #'eq :null))
                               (cl-ds.alg:distinct :test #'=
                                                   :hash-function #'identity)
                               cl-ds.alg:to-vector)))
    (if (emptyp all-target-steams)
        (cl-ds.alg:on-each all-ticks
                           (lambda (data &aux (tick (second data)))
                             (list* product-id demo-id sequence-length
                                    round sequence-id
                                    (gethash tick bullet-numbers)
                                    (append (cddr data)
                                            (target-position (demo-derive.counter-strike:make-position))))))
        (cl-ds.alg:cartesian (lambda (steam data &aux (tick (second data)) (old-steam (elt data 15)))
                               (setf data (copy-list data))
                               (setf (elt data 15) steam)
                               (unless (eql steam old-steam)
                                 (setf (elt data 3) :null))
                               (list* product-id demo-id sequence-length
                                      round sequence-id
                                      (gethash tick bullet-numbers)
                                      (append (cddr data)
                                              (find-target-position positions tick steam))))
                             all-target-steams
                             all-ticks))))


(serapeum:defalias target-position
  (juxt #'demo-derive.counter-strike:position-x
        #'demo-derive.counter-strike:position-y
        #'demo-derive.counter-strike:position-z
        #'demo-derive.counter-strike:position-pitch
        #'demo-derive.counter-strike:position-yaw
        #'demo-derive.counter-strike:position-vel0
        #'demo-derive.counter-strike:position-vel1
        #'demo-derive.counter-strike:position-vel2
        #'demo-derive.counter-strike:position-boolean-mask
        #'demo-derive.counter-strike:position-zoom-level))


(serapeum:defalias attacker-position
  (juxt #'demo-derive.counter-strike:position-vel0
        #'demo-derive.counter-strike:position-vel1
        #'demo-derive.counter-strike:position-vel2
        #'demo-derive.counter-strike:position-boolean-mask
        #'demo-derive.counter-strike:position-zoom-level
        #'demo-derive.counter-strike:position-health
        #'demo-derive.counter-strike:position-armor))


(defun find-target-position (positions tick steam)
  (let ((result (or (demo-derive.counter-strike:tick/steam-position positions tick steam)
                    (demo-derive.counter-strike:make-position))))
    (target-position result)))


(defun find-attacker-position (positions tick steam)
  "returns vel0 vel1 vel2 armor health boolean-mask zoom-level"
  (let ((result (or (demo-derive.counter-strike:tick/steam-position positions tick steam)
                    (demo-derive.counter-strike:make-position))))
    (attacker-position result)))


(defun gather-rounds (demo-id)
  (vellum:with-header ((vellum:make-header 'round-order 'terrorists 'counter-terrorists))
    (~> (vellum-postmodern:postgres-query
         `(:select round_order terrorist_steam64_ids counter_terrorist_steam64_ids
            :from ,(demo-derive.counter-strike:table-name "demos_demoround" demo-id))
         vellum.header:*header*)
        (cl-ds.alg:to-hash-table
         :hash-table-key (vellum:brr round-order)
         :hash-table-value (vellum:bind-row (terrorists counter-terrorists)
                             (list
                              (cl-ds:make-from-traversable terrorists
                                                           'cl-ds.sets.skip-list:mutable-skip-list-set
                                                           #'< #'=)
                              (cl-ds:make-from-traversable counter-terrorists
                                                           'cl-ds.sets.skip-list:mutable-skip-list-set
                                                           #'< #'=)))))))


(defun side (rounds round-order steam-id)
  (declare (optimize (debug 3)))
  (if (eq :null steam-id)
      :null
      (bind (((terrorists counter-terrorists) (gethash round-order rounds)))
        (cond ((cl-ds:at terrorists steam-id) 2)
              ((cl-ds:at counter-terrorists steam-id) 3)
              ((< (cl-ds:size terrorists) (cl-ds:size counter-terrorists)) 2)
              (t 3)))))


(defun current-round (&rest all)
  (declare (ignore all))
  (vellum:rr 'round))


(defun gather-wfhds (positions rounds demo-id product-id)
  (vellum:with-header ((vellum:make-header 'steam 'id 'tick 'round 'weapon-id
                                           'attacker-x 'attacker-y 'attacker-z
                                           'attacker-pitch 'attacker-yaw 'health-dmg 'armor-dmg
                                           'hitgroup 'target-steam 'playerhurt-id
                                           'health 'armor 'target-x 'target-y 'target-z))
    (~> (vellum-postmodern:postgres-query
         `(:order-by (:select fire.steam64_id (:as fire.id fire_id)
                       fire.current_tick fire.round_order fire.weapon_id
                       fire.x fire.y fire.z fire.pitch fire.yaw
                       hurt.dmg_health hurt.dmg_armor hurt.hitgroup
                       hurt.steam64_id (:as hurt.id playerhurt_id) hurt.health hurt.armor
                       (:as hurt.x target-x) (:as hurt.y target-y) (:as hurt.z target-z)
                       :from (:as ,(demo-derive.counter-strike:table-name "demos_weaponfire" demo-id)
                                  fire)
                       :left-join (:as ,(demo-derive.counter-strike:table-name "demos_demoplayerhurt" demo-id)
                                       hurt)
                       :on (:= hurt.weapon_fire_id fire.id))
                     current_tick)
         vellum.header:*header*)
        (cl-ds.alg:only (compose (rcurry #'gethash *weapon-type-intervals*)
                                 (rcurry #'gethash *weapon-types*))
                        :key (vellum:brr weapon-id))
        (cl-ds.alg:on-each (vellum:bind-row (id weapon-id attacker-x attacker-y attacker-z
                                                tick attacker-pitch attacker-yaw
                                                health-dmg steam armor-dmg hitgroup
                                                target-steam playerhurt-id round health armor
                                                target-x target-y target-z)
                             (append (list weapon-id                     ; 0
                                           tick                          ; 1
                                           id                            ; 2
                                           playerhurt-id                 ; 3
                                           steam                         ; 4
                                           attacker-x                    ; 5
                                           attacker-y                    ; 6
                                           attacker-z                    ; 7
                                           attacker-pitch                ; 8
                                           attacker-yaw                  ; 9
                                           health-dmg                    ; 10
                                           armor-dmg                     ; 11
                                           hitgroup                      ; 12
                                           (side rounds round steam)     ; 13
                                           (side rounds round target-steam) ; 14
                                           target-steam)                    ; 15
                                     (find-attacker-position positions tick steam)
                                     (list health armor))))
        (cl-ds.alg:group-by :key (vellum:brr round))
        (cl-ds.alg:group-by :key (vellum:brr steam))
        (cl-ds.alg:group-by :key (vellum:brr weapon-id))
        (cl-ds.alg:on-each (lambda (datum) (list (current-round) datum)))
        (cl-ds.alg:partition-if
         (lambda (previous current)
           (let* ((current-tick (second current))
                  (previous-tick (second previous))
                  (weapon-id (first current))
                  (weapon-type (gethash weapon-id *weapon-types*))
                  (tick-difference (- current-tick previous-tick))
                  (interval (gethash weapon-type *weapon-type-intervals*))
                  (in-interval-p (<= tick-difference interval)))
             in-interval-p))
         :key #'second)
        (cl-ds.alg:multiplex
         :function (curry #'all-target-positions-at-ticks
                          demo-id product-id positions))
        cl-ds.alg:to-vector)))


(defun flatten-wfhds (vectors)
  (~> vectors
      (cl-ds.alg:multiplex :key #'cdr)
      (cl-ds.alg:multiplex :key #'cdr)
      (cl-ds.alg:multiplex :key #'cdr)
      (vellum:to-table
       :columns `(product-id demo-id
                  sequence-length sequence-id
                  sequence-number weapon-fire-id
                  ,@*attacker-columns* ,@*target-columns*
                  round)
       :restarts-enabled nil)))


(defun obtain-data (demo-id product-id)
  (~> (demo-derive.common:memo-apply 'demo-derive.counter-strike:gather-positions
                                     demo-id)
      (gather-wfhds (gather-rounds demo-id)
                    demo-id
                    product-id)
      flatten-wfhds))


(defun load-data (data-frame)
  (vellum:copy-to :postmodern
                  'postprocessing.wfhds_product
                  data-frame))


(defun run (demo-id product-id)
  (~> (obtain-data demo-id product-id)
      load-data))
