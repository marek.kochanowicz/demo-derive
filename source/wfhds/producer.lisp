(cl:in-package #:demo-derive.wfhds)


(defclass wfhds-producer (demo-derive.produce:single-table-producer
                          demo-derive.produce:single-input-lisp-producer)
  ()
  (:default-initargs :name "wfhds"
                     :table-name 'postprocessing.wfhds_product
                     :path-to-schema (~>> (asdf:system-source-directory :demo-derive)
                                          (merge-pathnames "source/wfhds/schema.sql"))))


(defmethod demo-derive.produce:invoke-with-products-group/product/dependencies
    ((producer wfhds-producer)
     products-group
     product
     dependencies
     extra-arguments)
  (run (first-elt (demo-derive.produce:demo-id product))
       (demo-derive.produce:id product)))


(demo-derive.produce:define-lisp-producer *producer* (make 'wfhds-producer))
