create table if not exists postprocessing.position_product (
  product_id int4 not null,
  steam64_id int8 not null,
  foreign_key int4 not null,
  demo_id int4 NOT NULL,
  x real not NULL,
  y real not NULL,
  z real not NULL,
  pitch real not NULL,
  yaw real not NULL,
  vec_velocity_0 real not NULL DEFAULT 0.0,
  vec_velocity_1 real not NULL DEFAULT 0.0,
  vec_velocity_2 real not NULL DEFAULT 0.0,
  active_weapon_id int2 NULL,
  armor_value int2 NULL,
  health int2 NULL,
  zoom_level int2 NULL,
  boolean_mask int2 not null default 0,
  map_zone varchar,
  CONSTRAINT position_product_fk foreign key(product_id) references postprocessing.products on delete cascade
);

CREATE INDEX if not exists positions_index ON postprocessing.position_product using btree(demo_id, foreign_key,steam64_id,product_id);
grant all on postprocessing.position_product to public;
