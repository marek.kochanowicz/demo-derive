(cl:in-package #:demo-derive.position)


(defclass position-producer (demo-derive.produce:single-table-producer
                              demo-derive.produce:single-input-lisp-producer)
  ()
  (:default-initargs :name "position"
                     :table-name 'postprocessing.position_product
                     :path-to-schema (~>> (asdf:system-source-directory :demo-derive)
                                          (merge-pathnames "source/position/schema.sql"))))


(defun outline (map-name)
  (if-let ((map
            (switch (map-name :test 'equal)
              ("de_inferno" :inferno)
              ("de_cbble" nil)
              ("de_train" :train)
              ("de_ancient" :ancient)
              ("de_dust2" :dust)
              ("de_nuke" :nuke)
              ("de_mirage" :mirage)
              ("de_overpass" :overpass)
              ("de_vertigo" :vertigo)
              ("DE_INFERNO" :inferno))))
    (cs-maps:outline map)
    nil))


(defun find-map-name (demo-id)
  (first (first (postmodern:query (:select 'map_name :from 'demos_demo :where (:= 'id demo-id))))))


(defmethod demo-derive.produce:invoke-with-products-group/product/dependencies
    ((producer position-producer)
     products-group
     product
     dependencies
     arguments)
  (bind ((product-id (demo-derive.produce:id product))
         (demo-id (~> product demo-derive.produce:demo-id first-elt))
         (outline (outline (find-map-name demo-id)))
         ((k1 . source-table) (assoc "source_table" arguments :test 'equal))
         ((k2 . foregin-id) (assoc "id" arguments :test 'equal))
         ((k3 . current-tick) (assoc "current_tick" arguments :test 'equal))
         (allowed-difference (if-let ((slack (assoc "allowed_difference" arguments :test 'equal)))
                               (cdr slack)
                               10))
         (positions (demo-derive.common:memo-apply 'demo-derive.counter-strike:gather-positions
                                                   demo-id))
         (result-data-frame (vellum:make-table :columns '(product-id
                                                          steam-id
                                                          foregin-id
                                                          demo-id
                                                          x y z
                                                          pitch yaw
                                                          vel0 vel1 vel2
                                                          active-weapon-id
                                                          armor-value
                                                          health
                                                          zoom-level
                                                          boolean-mask
                                                          zone)))
         (index 0)
         ((:flet (setf at) (new-value column))
          (setf (vellum:at result-data-frame index column) new-value)))
    (declare (ignore k1 k2 k3))
    (assert source-table)
    (assert foregin-id)
    (assert current-tick)
    (postmodern:doquery (:select (make-symbol foregin-id) (make-symbol current-tick)
                          :from (demo-derive.counter-strike:table-name source-table demo-id))
        (foregin-id current-tick)
      (cl-ds:across positions
                    (lambda (steam.data &aux (steam (car steam.data)))
                      (when-let ((position (demo-derive.counter-strike:tick/steam-position
                                            positions current-tick steam
                                            :allowed-difference allowed-difference)))
                        (setf (at 'product-id) product-id
                              (at 'steam-id) steam
                              (at 'foregin-id) foregin-id
                              (at 'demo-id) demo-id
                              (at 'x) (demo-derive.counter-strike:position-x position)
                              (at 'y) (demo-derive.counter-strike:position-y position)
                              (at 'z) (demo-derive.counter-strike:position-z position)
                              (at 'pitch) (demo-derive.counter-strike:position-pitch position)
                              (at 'yaw) (demo-derive.counter-strike:position-yaw position)
                              (at 'vel0) (demo-derive.counter-strike:position-vel0 position)
                              (at 'vel1) (demo-derive.counter-strike:position-vel1 position)
                              (at 'vel2) (demo-derive.counter-strike:position-vel2 position)
                              (at 'active-weapon-id) (demo-derive.counter-strike:position-active-weapon position)
                              (at 'armor-value) (demo-derive.counter-strike:position-armor position)
                              (at 'health) (demo-derive.counter-strike:position-health position)
                              (at 'zoom-level) (demo-derive.counter-strike:position-zoom-level position)
                              (at 'boolean-mask) (demo-derive.counter-strike:position-boolean-mask position))
                        (when outline
                          (when-let ((zone (cs-maps:point->zone outline
                                                                (demo-derive.counter-strike:position-x position)
                                                                (demo-derive.counter-strike:position-y position)
                                                                (demo-derive.counter-strike:position-z position))))
                            (setf (at 'zone) (cs-maps:callout zone))))
                        (incf index)))))
    (vellum:copy-to :postmodern 'postprocessing.position_product
                    result-data-frame
                    :batch-size 2500)))


(demo-derive.produce:define-lisp-producer *producer* (make 'position-producer))
