create TABLE if not exists postprocessing.equipment_product (
  product_id int4,
  foreign_key int4,
  demo_id int4,
  steam64_id int8,
  currently_carried_items_ids int4[]
);

grant all on table postprocessing.equipment_product to public;
