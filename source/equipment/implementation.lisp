(cl:in-package #:demo-derive.equipment)


(defclass equipment-producer (demo-derive.produce:single-table-producer
                              demo-derive.produce:single-input-lisp-producer)
  ()
  (:default-initargs :name "equipment"
                     :table-name 'postprocessing.equipment_product
                     :path-to-schema (~>> (asdf:system-source-directory :demo-derive)
                                          (merge-pathnames "source/equipment/schema.sql"))))


(defmethod demo-derive.produce:invoke-with-products-group/product/dependencies
    ((producer equipment-producer)
     products-group
     product
     dependencies
     arguments)
  (bind ((product-id (demo-derive.produce:id product))
         (demo-id (~> product demo-derive.produce:demo-id first-elt))
         ((k1 . source-table) (assoc "source_table" arguments :test 'equal))
         ((k2 . foregin-id) (assoc "id" arguments :test 'equal))
         ((k3 . current-tick) (assoc "current_tick" arguments :test 'equal)))
    (declare (ignore k1 k2 k3))
    (assert source-table)
    (assert foregin-id)
    (assert current-tick)
    (postmodern:execute
     (format nil
             "insert into postprocessing.equipment_product select distinct on (steam64_id, dr.~a) ~a as product_id, dr.~a as foreign_key, eq.demo_id, eq.steam64_id, eq.currently_carried_items_ids from public.demos_equipment_~a as eq join public.~a_~a as dr on eq.current_tick <= dr.~a ORDER BY eq.steam64_id, dr.~a desc, eq.current_tick DESC, eq.id desc; "
             foregin-id
             product-id
             foregin-id
             demo-id
             source-table
             demo-id
             current-tick
             foregin-id))))


(demo-derive.produce:define-lisp-producer *producer* (make 'equipment-producer))
