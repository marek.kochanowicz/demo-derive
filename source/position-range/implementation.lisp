(cl:in-package #:demo-derive.position-range)


(defclass position-range-producer (demo-derive.produce:single-table-producer
                                   demo-derive.produce:single-input-lisp-producer)
  ()
  (:default-initargs :name "position_range"
                     :table-name 'postprocessing.position_product
                     :path-to-schema (~>> (asdf:system-source-directory :demo-derive)
                                          (merge-pathnames "source/position-range/schema.sql"))))


(defmethod demo-derive.produce:invoke-with-products-group/product/dependencies
    ((producer position-range-producer)
     products-group
     product
     dependencies
     arguments)
  (bind ((product-id (demo-derive.produce:id product))
         (demo-id (~> product demo-derive.produce:demo-id first-elt))
         ((k1 . source-table) (assoc "source_table" arguments :test 'equal))
         ((k2 . foregin-id) (assoc "id" arguments :test 'equal))
         ((k3 . start-tick) (assoc "start_tick" arguments :test 'equal))
         ((k4 . end-tick) (assoc "end_tick" arguments :test 'equal))
         (before (or (cdr (assoc "before" arguments :test 'equal))
                     0))
         (after (or (cdr (assoc "after" arguments :test 'equal))
                    0))
         (positions (demo-derive.common:memo-apply 'demo-derive.counter-strike:gather-positions
                                                   demo-id))
         (result-data-frame-columns '(product-id
                                      current-tick
                                      steam-id
                                      foregin-id
                                      demo-id
                                      x y z
                                      pitch yaw
                                      vel0 vel1 vel2
                                      active-weapon-id
                                      armor-value
                                      health
                                      zoom-level
                                      boolean-mask))
         (index 0)
         (result-data-frame nil)
         ((:flet (setf at) (new-value column))
          (setf (vellum:at result-data-frame index column) new-value)))
    (declare (ignore k1 k2 k3 k4))
    (assert source-table)
    (assert foregin-id)
    (assert start-tick)
    (assert end-tick)
    (map nil
         (lambda (row)
           (bind (((foregin-id start-tick end-tick) row))
             (cl-ds:across positions
                           (lambda (steam.data &aux (steam (car steam.data)))
                             (setf index 0)
                             (setf result-data-frame
                                   (vellum:make-table :columns result-data-frame-columns))
                             (cl-ds:traverse
                              (demo-derive.counter-strike:tick/steam-position-range positions start-tick end-tick steam
                                                                                    :before before
                                                                            :after after)
                              (lambda (tick.position)
                                (bind (((tick . position) tick.position))
                                  (setf (at 'current-tick) tick
                                        (at 'product-id) product-id
                                        (at 'steam-id) steam
                                        (at 'foregin-id) foregin-id
                                        (at 'demo-id) demo-id
                                        (at 'x) (demo-derive.counter-strike:position-x position)
                                        (at 'y) (demo-derive.counter-strike:position-y position)
                                        (at 'z) (demo-derive.counter-strike:position-z position)
                                        (at 'pitch) (demo-derive.counter-strike:position-pitch position)
                                        (at 'yaw) (demo-derive.counter-strike:position-yaw position)
                                        (at 'vel0) (demo-derive.counter-strike:position-vel0 position)
                                        (at 'vel1) (demo-derive.counter-strike:position-vel1 position)
                                        (at 'vel2) (demo-derive.counter-strike:position-vel2 position)
                                        (at 'active-weapon-id) (demo-derive.counter-strike:position-active-weapon position)
                                        (at 'armor-value) (demo-derive.counter-strike:position-armor position)
                                        (at 'health) (demo-derive.counter-strike:position-health position)
                                        (at 'zoom-level) (demo-derive.counter-strike:position-zoom-level position)
                                        (at 'boolean-mask) (demo-derive.counter-strike:position-boolean-mask position)))
                                (incf index)))
                             (vellum:copy-to :postmodern 'postprocessing.position_range_product
                                             result-data-frame
                                             :batch-size 2500)))))
         (postmodern:query (:select (make-symbol foregin-id) (make-symbol start-tick) (make-symbol end-tick)
                             :from (demo-derive.counter-strike:table-name source-table demo-id))))))


(demo-derive.produce:define-lisp-producer *producer* (make 'position-range-producer))
