(cl:defpackage #:demo-derive.counter-strike
  (:use #:cl #:demo-derive.aux-package)
  (:export
   #:gather-positions
   #:table-name
   #:tick/steam-position
   #:tick/steam-position-range
   #:position-active-weapon
   #:position-armor
   #:position-health
   #:position-x
   #:position-y
   #:position-z
   #:position-pitch
   #:position-yaw
   #:position-vel0
   #:position-vel1
   #:position-vel2
   #:position-boolean-mask
   #:position-zoom-level
   #:make-position))
