(cl:in-package #:demo-derive.counter-strike)


(defun table-name (core demo-id)
  (make-symbol (string-upcase (format nil "~a_~a" core demo-id))))


(defun gather-positions (demo-id)
  (vellum:with-header ((vellum.header:make-header
                        'steam-id 'tick 'x 'y 'z 'pitch 'yaw
                        'target-vel0 'target-vel1 'target-vel2
                        'target-armor 'target-health 'target-boolean-mask
                        'zoom-level 'active-weapon))
    (~> (vellum-postmodern:postgres-query
         `(:select steam64_id current_tick x y z pitch yaw
            vec_velocity_0 vec_velocity_1 vec_velocity_2
            armor_value health boolean_mask zoom_level active_weapon_id
            :from ,(table-name "demos_demoplayerposition" demo-id))
         vellum.header:*header*)
        (cl-ds.alg:group-by :key (vellum:brr steam-id))
        (cl-ds.alg:on-each (vellum:bind-row (tick
                                             target-armor target-health
                                             x y z
                                             pitch yaw
                                             target-vel0 target-vel1 target-vel2
                                             target-boolean-mask
                                             zoom-level active-weapon)
                             (cons tick (vector target-armor target-health
                                                x y z pitch yaw
                                                target-vel0 target-vel1 target-vel2
                                                target-boolean-mask
                                                zoom-level active-weapon))))
        (cl-ds.alg:to-vector
         :after (lambda (vector) (sort vector #'> :key #'car))))))


(defstruct (position (:type vector))
  (armor :null)
  (health :null)
  (x :null)
  (y :null)
  (z :null)
  (pitch :null)
  (yaw :null)
  (vel0 :null)
  (vel1 :null)
  (vel2 :null)
  (boolean-mask :null)
  (zoom-level :null)
  (active-weapon :null))


(defun tick/steam-position-range (positions start-tick end-tick steam
                                  &key
                                    (before 0)
                                    (after 0))
  (declare (optimize (speed 3) (safety 0))
           (type fixnum before after start-tick end-tick steam))
  (when (eq :null steam)
    (return-from tick/steam-position-range (cl-ds:xpr () nil)))
  (let ((dict (the (or null (array t (*)))
                   (cl-ds:at positions steam))))
    (when (null dict)
      (return-from tick/steam-position-range (cl-ds:xpr () nil)))
    (assert (array-has-fill-pointer-p dict))
    (let ((lower-bound (cl-ds.utils:lower-bound dict
                                                (the fixnum (+ end-tick after))
                                                #'>
                                                :key #'car))
          (length (length dict)))
      (declare (type fixnum length lower-bound))
      (cl-ds:xpr (:i lower-bound)
        (declare (type fixnum i))
        (when (< i length)
          (let ((key.value (aref dict i)))
            (when (<= (the fixnum (- start-tick before))
                      (the fixnum (car key.value))
                      (the fixnum (+ end-tick after)))
              (cl-ds:send-recur key.value :i (the fixnum (1+ i))))))))))


(defun tick/steam-position (positions tick steam &key (allowed-difference 10))
  (declare (optimize (speed 3) (safety 0))
           (type fixnum tick steam allowed-difference))
  (when (eq :null steam)
    (return-from tick/steam-position nil))
  (let ((dict (the (or null (array t (*)))
                   (cl-ds:at positions steam))))
    (when (null dict)
      (return-from tick/steam-position nil))
    (assert (array-has-fill-pointer-p dict))
    (let* ((lower-bound (cl-ds.utils:lower-bound dict
                                                 tick
                                                 #'>
                                                 :key #'car))
           (length (length dict))
           (found (< lower-bound length)))
      (declare (type fixnum lower-bound length)
               (type boolean found))
      (if (or (null allowed-difference)
              (and found
                   (<= (- tick (the fixnum (car (aref dict lower-bound))))
                       allowed-difference)))
          (cdr (aref dict lower-bound))
          nil))))
