(cl:in-package #:demo-derive.map-zone)


(defun outline (map-name)
  (if-let ((map
            (switch (map-name :test 'equal)
              ("de_inferno" :inferno)
              ("de_cbble" nil)
              ("de_train" :train)
              ("de_ancient" :ancient)
              ("de_dust2" :dust)
              ("de_nuke" :nuke)
              ("de_mirage" :mirage)
              ("de_overpass" :overpass)
              ("de_vertigo" :vertigo)
              ("DE_INFERNO" :inferno))))
    (cs-maps:outline map)
    nil))


(defun find-map-name (demo-id)
  (first (first (postmodern:query (:select 'map_name :from 'demos_demo :where (:= 'id demo-id))))))


(defclass map-zone-producer (demo-derive.produce:single-table-producer
                             demo-derive.produce:single-input-lisp-producer)
  ()
  (:default-initargs :name "map_zone"
                     :table-name 'postprocessing.map_zone_product
                     :path-to-schema (~>> (asdf:system-source-directory :demo-derive)
                                          (merge-pathnames "source/map-zone/schema.sql"))))


(defmethod demo-derive.produce:invoke-with-products-group/product/dependencies
    ((producer map-zone-producer)
     products-group
     product
     dependencies
     arguments)
  (bind ((product-id (demo-derive.produce:id product))
         (demo-id (~> product demo-derive.produce:demo-id first-elt))
         (outline (outline (find-map-name demo-id)))
         ((k1 . source-table) (assoc "source_table" arguments :test 'equal))
         ((k2 . foregin-id) (assoc "id" arguments :test 'equal))
         ((k3 . kx) (assoc "x" arguments :test 'equal))
         ((k4 . ky) (assoc "y" arguments :test 'equal))
         ((k5 . kz) (assoc "z" arguments :test 'equal))
         (result-data-frame (vellum:make-table :columns
                                               '(product-id foreign-id zone)))
         (index 0)
         ((:flet (setf at) (new-value column))
          (setf (vellum:at result-data-frame index column) new-value)))
    (declare (ignore k1 k2 k3 k4 k5))
    (assert source-table)
    (assert foregin-id)
    (assert kx)
    (assert ky)
    (assert kz)
    (when outline
      (postmodern:doquery (:select (make-symbol foregin-id)
                            (make-symbol kx)
                            (make-symbol ky)
                            (make-symbol kz)
                            :from (demo-derive.counter-strike:table-name source-table demo-id))
          (foreign-id x y z)
        (when-let ((zone (cs-maps:point->zone outline x y z)))
          (setf (at 'foreign-id) foreign-id
                (at 'product-id) product-id
                (at 'zone) (cs-maps:callout zone))
          (incf index)))
      (vellum:copy-to :postmodern 'postprocessing.map_zone_product
                      result-data-frame
                      :batch-size 2500))))
