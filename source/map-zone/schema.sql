create table if not exists postprocessing.map_zone_product (
  product_id int4 not null,
  foreign_key int4 not null,
  zone varchar not null,
  CONSTRAINT map_zone_product foreign key(product_id) references postprocessing.products on delete cascade
);

CREATE INDEX if not exists map_zone_index ON postprocessing.map_zone_product using btree(product_id, foreign_key);
grant all on postprocessing.map_zone_product to public;
