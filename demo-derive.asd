(asdf:defsystem #:demo-derive
  :name "demo-derive"
  :description "Manage data produced from the demos."
  :version "0.0.0"
  :author "Marek Kochanowicz"
  :depends-on ( :iterate
                :serapeum
                :metabang-bind
                :cl-data-structures
                :postmodern-localtime
                :trivial-garbage
                :alexandria
                :vellum
                :demo-derive-agnostic
                :vellum-postmodern
                :cs-maps
                :cl-progress-bar
                :postmodern)
  :serial T
  :pathname "source"
  :components ((:module "counter-strike"
                :components ((:file "package")
                             (:file "positions")))
               (:module "map-zone"
                :components ((:file "package")
                             (:file "implementation")
                             (:file "producer")))
               (:module "wfhds"
                :components ((:file "package")
                             (:file "implementation")
                             (:file "producer")))
               (:module "equipment"
                :components ((:file "package")
                             (:file "implementation")))
               (:module "position"
                :components ((:file "package")
                             (:file "implementation")))
               (:module "position-range"
                :components ((:file "package")
                             (:file "implementation")))))
